<automate-toolbar>
    <section class="hero is-fullheight" style="min-height: 94vh;">
        <automate-header ctrl={this.opts.controller} parent={this}></automate-header>
        <automate-content ctrl={this.opts.controller} parent={this}></automate-content>
        <automate-footer ctrl={this.opts.controller} parent={this}></automate-footer>
    </section>
    <script>
      import './automate-header.tag'
      import './automate-content.tag'
      import './automate-footer.tag'
    </script>
</automate-toolbar>
